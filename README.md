# gitlab-vagrant

This repo provides a template Vagrantfile to create a CoreOS virtual machine and install gitlab-ce and gitlab-runner.

1) Clone this project and get it running

```
git clone https://gitlab.com/hsuchan/gitlab-vagrant.git
cd gitlab-vagrant
vagrant up
vagrant ssh
```

2) Login to gitlab-ce

`http://172.17.8.101`

- Change your password
- Register
- Create a new project

3) Setup gitlab-runner

In the runners section, you should see something similar to this:

```
How to setup a specific Runner for a new project
1. Install a Runner compatible with GitLab CI (checkout the GitLab Runner section for information on how to install it).
2. Specify the following URL during the Runner setup: http://172.17.8.101/ci
3. Use the following registration token during setup: hcxn_qtbTESkhPGYE2mM
4. Start the Runner!
```

4) Register gitlab-runner

```
docker exec -it gitlab-runner \
  gitlab-ci-multi-runner register \
  --non-interactive \
  --url "http://172.17.8.101/ci" \
  --registration-token "hcxn_qtbTESkhPGYE2mM" \
  --tag-list "Docker" \
  --run-untagged="false" \
  --executor "docker" \
  --docker-image "ruby:2.1"
```

References:

`https://github.com/coreos/coreos-vagrant`

`https://docs.gitlab.com/ce/ci/docker/using_docker_images.html`
